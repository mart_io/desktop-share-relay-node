var mediaConstraints = {
	mandatory: {
		OfferToReceiveAudio: true,
		OfferToReceiveVideo: true
	}
};

var VideoChat = {
	socket: io(),
	candidateList: [],

	requestMediaStream: function(event) {
		getUserMedia(
			{video:true, audio:true},
			VideoChat.onMediaStream,
			VideoChat.noMediaStream
		);
	},

	onMediaStream: function(stream) {
		console.log("MediaStream");
		VideoChat.localVideo = document.getElementById('local-video');
		VideoChat.localVideo.volume = 0;
		VideoChat.localStream = stream;
		VideoChat.videoButton.setAttribute('disabled', 'disabled');
		attachMediaStream(VideoChat.localVideo, stream);

		// Signalling
		VideoChat.socket.emit('join', 'test'); // Roomname: test
		VideoChat.socket.on('ready', VideoChat.readyToCall);
		VideoChat.socket.on('offer', VideoChat.onOffer);
		VideoChat.socket.on('candidate', VideoChat.onCandidate);
	},

	noMediaStream: function() {
		console.log("No media stream");
	},

	readyToCall: function(event) {
		VideoChat.callButton.removeAttribute('disabled');
	},

	startCall: function(event) {
		console.log('startCall()');
		VideoChat.socket.emit('startVideoCall');
		VideoChat.socket.on('token', VideoChat.onToken(VideoChat.createOffer));
		VideoChat.socket.emit('token');
	},

	createOffer: function() {
		console.log("createOffer");

		VideoChat.peerConnection.createOffer(
			function(offer) {
				console.log(offer);
				VideoChat.peerConnection.setLocalDescription(offer, VideoChat.onSuccess("setLocalDescription"), VideoChat.onFailure("setLocalDescription"));
				VideoChat.socket.emit('offer', JSON.stringify(offer));
			},
			function(err) {
				console.log(err);
			}
		);
	},

	createAnswer: function(offer) {
		console.log("createAnswer");
		console.log(offer);
		return function() {
			var rtcOffer = new RTCSessionDescription(JSON.parse(offer));
			VideoChat.peerConnection.setRemoteDescription(rtcOffer, VideoChat.onSuccess("setRemoteDescription"), VideoChat.onFailure("setRemoteDescription"));

			// Can only be done after remote description is set
			for(var i=0 ; i < VideoChat.candidateList.length; ++i) {
				var candidate = VideoChat.candidateList[i];
				VideoChat.peerConnection.addIceCandidate(candidate, VideoChat.onSuccess("addIceCandidate"), VideoChat.onFailure("addIceCandidate"));
			}


			VideoChat.peerConnection.createAnswer(
				function(answer) {
					VideoChat.peerConnection.setLocalDescription(answer, VideoChat.onSuccess("setLocalDescription"), VideoChat.onFailure("setLocalDescription"));
					VideoChat.socket.emit('answer', JSON.stringify(answer));
				},
				function(err) {
					console.log(err);
				},
				mediaConstraints
			);
		}
	},

	onToken: function(callback) {
		return function(token) {
			console.log("onToken");
			console.log(token);
			var twilioOptions = {
				iceServers: token.iceServers
			};

			var googOptions = {
				"iceServers": [{"url":"stun:stun.l.google.com:19302"}]
			};

			var localOptions = {
				"iceServers": [
					{"url":"stun:108.35.1.197:3478"}, 
					{"url":"turn:108.35.1.197:3478", "credential":"password", "username":"username"}, 
				]
			};
			VideoChat.peerConnection = new RTCPeerConnection(twilioOptions);
			VideoChat.peerConnection.onicecandidate = VideoChat.onIceCandidate;
			VideoChat.peerConnection.oniceconnectionstatechange = function(e) {
				console.log(e);
			};
			VideoChat.peerConnection.onaddstream = VideoChat.onAddStream;
			VideoChat.peerConnection.onremovestream = VideoChat.onRemoveStream;
			VideoChat.socket.on('candidate', VideoChat.onCandidate);
			VideoChat.socket.on('answer', VideoChat.onAnswer);
			VideoChat.peerConnection.addStream(VideoChat.localStream);
			callback();
		}
	},

	// Called when we receive remote stream
	onAddStream: function(event) {
		console.log("Adding stream");
		VideoChat.remoteVideo = document.getElementById('remote-video');
		attachMediaStream(VideoChat.remoteVideo, event.stream);
	},

	onRemoveStream: function(event) {
		console.log("onRemoveStream");
	},

	onAnswer: function(answer) {
		console.log("got Answer");
		console.log(answer);
		var rtcAnswer = new RTCSessionDescription(JSON.parse(answer));
		VideoChat.peerConnection.setRemoteDescription(rtcAnswer, VideoChat.onSuccess("setRemoteDescription"), VideoChat.onFailure("setRemoteDescription"));
	},

	onCandidate: function(candidate) {
		console.log("Received candidate");
		console.log(candidate);
		var rtcCandidate = new RTCIceCandidate(JSON.parse(candidate));
		console.log(rtcCandidate);
		if(VideoChat.peerConnection) {
			VideoChat.peerConnection.addIceCandidate(rtcCandidate, VideoChat.onSuccess("addIceCandidate"), VideoChat.onFailure("addIceCandidate"));
		}
		else {
			VideoChat.candidateList.push(rtcCandidate);
		}
	},

	onIceCandidate: function(event) {
		if(event.candidate) {
			var candidate = event.candidate;
//			{"sdpMLineIndex":0,"sdpMid":"audio","candidate":"candidate:4278134664 1 udp 2122260223 192.168.1.8 51620 typ host generation 0"}
			console.log("Generated candidate");
			console.log(event.candidate);
			// JSON.stringify(event.candidate) doesn't work because that is a native object when using the Temasys plugin
			var payload = {
				"sdpMLineIndex":candidate.sdpMLineIndex,
				"sdpMid":candidate.sdpMid,
				"candidate":candidate.candidate
			};
			VideoChat.socket.emit('candidate', JSON.stringify(payload));
		} else {
			console.log("End of ICE /candidates");
		}

	},

	onSuccess: function(message) {
		return function() {
			console.log("SUCCESS: " + message);
		};
	},

	onFailure: function(message) {
		return function(err) {
			console.log("FAIL: " + message);
			console.log(err);
		};
	},

	onOffer: function(offer) {
		VideoChat.socket.on('token', VideoChat.onToken(VideoChat.createAnswer(offer)));
		VideoChat.socket.emit('token');
	}
};

VideoChat.videoButton = document.getElementById('get-video');
VideoChat.callButton = document.getElementById('call');

VideoChat.videoButton.addEventListener('click', VideoChat.requestMediaStream, false);
VideoChat.callButton.addEventListener('click', VideoChat.startCall, false);



