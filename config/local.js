var config = require('./global');

config.Environment = process.env.NODE_ENV || 'local';

module.exports = config;
