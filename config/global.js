var config = module.exports = {};

config.Environment = process.env.NODE_ENV || 'local';
config.SiteHost = 'http://localhost';
config.SitePort = process.env.PORT || 5000;
config.RedisURL = process.env.REDIS_URL || "redis://localhost:6379/0";
config.ViewEngine = 'vash';
config.ViewEngineFileExtension = 'vash';
config.MaximumUploadSize = 20000000;	// 20MB
config.CacheStrategy = './lib/hash_cache';
config.DataProvider = './lib/mock_data_provider';
config.CheckExpiredMeetingsInterval = 5000;
config.TwilioSID = process.env.TWILIO_ACCOUNT_SID || "ACa442cae461eb48b3988e91ac24b142d8";
config.TwilioToken = process.env.TWILIO_AUTH_TOKEN || "ebf06a9c4ce2ec9928abab11f955c879";

