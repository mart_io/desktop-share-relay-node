var config = require('./global');

config.Environment = process.env.NODE_ENV || 'AWS';

module.exports = config;
