var config = require('./global');

config.Environment = process.env.NODE_ENV || 'Heroku';
config.SiteHost = 'https://mart-io-desktopshare.herokuapp.com';

module.exports = config;
