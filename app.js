global.__base = __dirname + '/';

// bootstrap
var env = process.env.NODE_ENV || 'local';
var ConfigurationSettings = require('./config/' + env);

// package includes
var lwip = require('lwip');
var twilio = require('twilio')(ConfigurationSettings.TwilioSID, ConfigurationSettings.TwilioToken);
var path = require('path');
var Hapi = require('hapi');
var Blipp = require('blipp');
var cookieParser = require('socket.io-cookie-parser');
var CacheService = require(ConfigurationSettings.CacheStrategy);
var DataProvider = require(ConfigurationSettings.DataProvider);
var MeetingManager = require("./lib/meeting_manager")(ConfigurationSettings, CacheService, DataProvider);

function initRedis() {
    var rtg = require('url').parse(ConfigurationSettings.RedisURL);
    var redis = require('redis').createClient(rtg.port, rtg.hostname);
    if(rtg.auth){
        redis.auth(rtg.auth.split(":")[1]);
    }

    if(rtg.path) {
        redis.select(rtg.path.replace("/", ""));
    }

    return redis;
}

function initServer() {
	var server = new Hapi.Server();
    exports.server = server;

    var sopts = {};
    sopts.port = ConfigurationSettings.SitePort;

    if(process.env.HOST) {
        sopts.host = process.env.HOST;
    }
    server.connection(sopts);
	
	var viewEngineConfig = {};
	viewEngineConfig[ConfigurationSettings.ViewEngineFileExtension] = require(ConfigurationSettings.ViewEngine);
	
	server.views({
		engines: viewEngineConfig,
		path: path.join(__dirname, './views')
	});
	
	server.register({
		register: Blipp
	}, function (err) {
		if (err) {
			console.error("Failed to load a plugin:", err);
		}
		else {
			server.start(function () {
				console.log("server running at: ", server.info.uri);
			});
		}
	});
	
	return server;
}

function initSocketIO(server) {
	var io = require("socket.io")(server.listener);
	io.use(cookieParser());
	
	io.sockets.on('connection', function(socket) {
		console.log("New connection. Socket ID: " + socket.id);
	});
	return io;
}

var redis = null;
var server = initServer();
var io = initSocketIO(server);

// wire-up route and Socket.IO handlers
require("./lib/route_handlers")(server, ConfigurationSettings, MeetingManager);
require("./lib/video_call_handlers")(io, redis, twilio);
require("./lib/desktop_sharing_handlers")(io, MeetingManager);
