var Meeting = function(meeting_id, start_date, end_date, presenter_id) {
  this.meeting_id = meeting_id;
  this.start_date = start_date;
  this.end_date = end_date;
  this.presenter_id = presenter_id;
  this.current_frame = null;
  this.current_frame_delta = null;
  this.current_frame_delta_x = 0;
  this.current_frame_delta_y = 0;
  this.sequence = 0;
}

Meeting.prototype = {
  meeting_id: null,
  start_date: null,
  end_date: null,
  presenter_id: null,
  current_frame: null,
  
  is_meeting_active: function() {
    return (Date() > meeting.start_date && Date() < meeting.end_date);
  }
};

module.exports = Meeting;