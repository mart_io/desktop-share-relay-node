var ScreenshotFrame = require("./screenshot_frame");
var fs = require('fs');
var lwip = require('lwip');

module.exports = function (server, configurationSettings, meetingManager) {
	var configurationSettings = configurationSettings;
	var meetingManager = meetingManager;
	
	server.route({
		method: 'GET',
		path: '/{path*}',
		handler: {
			directory: { path: './public/', listing: false, index: true }
		}
	});

	server.route({
		method: 'GET',
		path: '/m/{meeting_id}',
		handler: function(request, reply) {
			console.log("meeting handler");
			console.log(request.state.martio_session);
			reply("Joining " + request.params.meeting_id);
		}
	});
	
	server.route({
		method: 'GET',
		path: '/client/{meeting_id}',
		handler: function(request, reply) {
			var context = {
				meeting_id : request.params.meeting_id,
				host : configurationSettings.SiteHost + ":" + configurationSettings.SitePort
			};
			reply.view('client', context);
		}
	});
	
	server.route({
		method: 'GET',
		path: '/host/{meeting_id}',
		handler: function(request, reply) {
			var context = {
				meeting_id : request.params.meeting_id,
				host : configurationSettings.SiteHost + ":" + configurationSettings.SitePort
			};
			reply.view('host', context);
		}
	});

    server.route({
        method: 'GET',
        path: '/client/{meeting_id}/current',
        handler: function (request, reply) {
            var meeting_id = request.params.meeting_id;
            var meeting = meetingManager.get_meeting(meeting_id);

            if (!meeting) {
                reply(new Object({ code: 404, error: "Invalid meeting ID" })).code(404);
                return;
            }
			
			if (!meeting.current_frame) {
				reply(new Object({ code: 405, error: "Error: initial screenshot not received yet" })).code(405);
				return;
			}
			
			reply(meeting.current_frame).header('Content-type', "image/png").code(200);
        }
    });
	
	server.route({
        method: 'GET',
        path: '/client/{meeting_id}/current_delta',
        handler: function (request, reply) {
            var meeting_id = request.params.meeting_id;
            var meeting = meetingManager.get_meeting(meeting_id);

            if (!meeting) {
                reply(new Object({ code: 404, error: "Invalid meeting ID" })).code(404);
                return;
            }
			
			if (!meeting.current_frame_delta) {
				reply(new Object({ code: 405, error: "Error: initial screenshot not received yet" })).code(405);
				return;
			}
			
			reply(meeting.current_frame_delta)
				.header('sequence', meeting.sequence)
				.header('rect_x', meeting.current_frame_delta_x)
				.header('rect_y', meeting.current_frame_delta_y)
				.header('Content-type', "image/png").code(200);
        }
    });
	
	server.route({
		method: 'POST',
		path: '/update_frame/{meeting_id}',
		config: {
			payload:{
				maxBytes: configurationSettings.MaximumUploadSize,
				output: 'data',
				parse: false
			}, 
			handler: function(request, reply) {
				var meeting_id = request.params.meeting_id;
				var meeting = meetingManager.get_meeting(meeting_id);
				
				if(!meeting) {
					reply(new Object({ code: 404, error: "Invalid meeting ID" })).code(404);
					return;
				}
				
				if(!meeting.start_date || (Date() < meeting.start_date)) {
					reply(new Object({ code: 412, error: ("Meeting doesn't start until " + meeting.start_date) })).code(412);
					return;
				}
				
				var rectx, recty, width, height, sequence, is_initial, updatedScreenshotBytes;
				
				try 
				{
					rectx = parseInt(request.headers.rect_x);
					recty = parseInt(request.headers.rect_y);
					width = parseInt(request.headers.rect_width);
					height = parseInt(request.headers.rect_height);
					sequence = Number(request.headers.sequence);
					is_initial = JSON.parse(request.headers.is_initial);
					updatedScreenshotBytes = new Buffer(request.payload);
					
					if(!updatedScreenshotBytes || updatedScreenshotBytes.length == 0) {
						reply(new Object({ code: 400, error: "0 bytes received in message body" })).code(400);
						return;
					}
				}
				catch(ex) {
					console.log("Header parsing failed for meeting: " + meeting_id);
					console.log(ex);
					reply(new Object({ code: 400, error: ex })).code(400);
					return;
				}
				
				if(sequence != meeting.sequence) {
					// sequence numbers somehow got out of order, so image will be corrupted now if we
					// apply deltas out of sequence or are missing any in-between
					console.log("Meeting updates arrived out of order; current seq: " + meeting.sequence + " received seq: " + sequence + " RESETTING sequence");
					reply(new Object({ code: 400, error: ("Meeting updates arrived out of order; current seq: " + meeting.sequence + " received seq: " + sequence) })).code(405);
					return;
				}
				
				if(is_initial) {
					meeting.sequence = 1;
					meeting.current_frame = updatedScreenshotBytes;
					meeting.current_frame_delta = updatedScreenshotBytes;
					meeting.current_frame_delta_x = 0;
					meeting.current_frame_delta_y = 0;
					console.log("Received INITIAL screenshot " + request.payload.length);
					reply().code(201);
					return;
				}
				
				// TODO: WIP ..add the lwip stuff to meetingManager.update_screenshot  
				//var updateRequest = new ScreenshotFrame(image, rectx, recty, width, height, sequence);
				//meetingManager.update_screenshot(meeting_id, updateRequest);
				
				lwip.open(meeting.current_frame, "png", function(err, current_image) 
				{ 
					lwip.open(updatedScreenshotBytes), "png",  function(err, delta) {
						var current_width = current_image.width();
						var current_height = current_image.height();
						var delta_width = delta.width();
						var delta_height = delta.height();
						
						// check that the total width and height of the delta frame don't exceed the current
						// screenshot dimensions or else we are in a bad state and will signal to start over
						var dimensionError = "";
						if(rectx + delta_width > current_width) {
							dimensionError = "Malformed width dimension: current width => " + current_width + " delta width => " + delta_width + " rect X => " + rectx;
						}
						else if (recty + delta_height > current_height) {
							dimensionError = dimensionError.length > 0 ? dimensionError + "\n" : dimensionError; 
							dimensionError = dimensionError + "Malformed height dimension: current height => " + current_height + " delta height => " + delta_height + " rect X => " + recty;
						}
						
						if(dimensionError.length > 0) {
							console.log(dimensionError);
							reply(new Object({ code: 400, error: dimensionError })).code(400);
							return;
						}
						
						current_image.paste(rectx, recty, delta, function(err, combinedImage) {
							if (err) {
								console.log("Error ocurred combining delta with current screenshot for meeting: : " + meeting_id);
								console.log(err); 
								reply(new Object({ code: 400, error: err })).code(400);
								return;
							}
							
							combinedImage.toBuffer('png', {quality: 90}, function(err, buffer){
								if (err) {
									console.log("Error ocurred compressing combined screenshot to PNG for meeting: " + meeting_id);
									console.log(err); 
									reply(new Object({ code: 400, error: err })).code(400);
									return;
								}
								
								meeting.sequence++;
								meeting.current_frame = buffer;
								meeting.current_frame_delta = updatedScreenshotBytes;
								meeting.meeting.current_frame_delta_x = rectx;
								meeting.meeting.current_frame_delta_y = recty;
								 	
								console.log("Received " + updatedScreenshotBytes.length + " bytes with bounding rectangle (" + rectx + "," + recty + ") [" + delta_width + "x" + delta_height + "]");
								reply().code(201);
							});
						});
					}
				});
			}
		}
	});
};
