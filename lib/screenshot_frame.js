var ScreenshotFrame = function(pixelArray, rectX, rectY, width, height, sequence) {
  this.PixelArray = pixelArray;
  this.X = rectX;
  this.Y = rectY;
  this.Width = width;
  this.Height = height;
  this.SequenceNumber = sequence;
}

ScreenshotFrame.prototype = {
  PixelArray : null,
  X : 0,
  Y : 0,
  Width : 0,
  Height : 0,
  SequenceNumber : 0
};

module.exports = ScreenshotFrame;