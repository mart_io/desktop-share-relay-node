var Meeting = require("./meeting");

meetings_mock_data = {};
meetings_mock_data["ABC"] = new Meeting("ABC", new Date(), new Date("December 31, 2099 00:00:00"), 1);
meetings_mock_data["DEF"] = new Meeting("DEF", new Date(new Date().getTime() + (15*60000)), new Date("December 31, 2099 00:00:00"), 1);	// starts in 15min to simulate preview mode
meetings_mock_data["GHI"] = new Meeting("GHI", new Date(), new Date(new Date().getTime() + (0.25*60000)), 1);							// meeting will end in 30 seconds from app start

module.exports = {
    get_meeting: function(meeting_id) {
		return meetings_mock_data[meeting_id];
	}
};
