var Meeting = require("./meeting");
var Events = require('events');
var lwip = require('lwip');

module.exports = function (ConfigurationSettings, CacheProvider, DataProvider) {
	var module = {};
	var loaded_meeting_keys = {}; // maintain this list in cache later as this won't work with multiple Node servers - duh
	var events = new Events.EventEmitter();

	module.get_meeting = function (meeting_id) {
		var cache_key = "mtng_mgr:" + meeting_id;
		var meeting = CacheProvider.get(cache_key);
		if (!meeting) {
			meeting = DataProvider.get_meeting(meeting_id);
			CacheProvider.set(cache_key, meeting);
			loaded_meeting_keys[meeting_id] = true;
		}
		return meeting;
	};

	module.update_screenshot = function(meeting_id, screenshot)
	{
		var meeting = module.get_meeting(meeting_id);
		if(!meeting) return;

		meeting.current_frame = screenshot;
		meeting.sequence++;
		var cache_key = "mtng_mgr:" + meeting_id;
		CacheProvider.set(cache_key, meeting);
		
		events.emit('screenshot_updated', meeting_id, screenshot);
	};
	
	module.get_expired_sessions = function () {
		var expired_session_ids = [];
		var meeting;
		for (var key in loaded_meeting_keys) {
			meeting = module.get_meeting(key);
			if (meeting && new Date().getTime() > meeting.end_date.getTime()) {
				expired_session_ids.push(key);
				delete loaded_meeting_keys[key]; // remove from active set of sessions being monitored
			}
		}
		return expired_session_ids;
	}

	// constantly check if any meetings have expired and if they have, disconnect any clients
	// attached to the Socket.IO room of the expired meeting ID 
	setInterval(function () {
			var expired_meetings = module.get_expired_sessions(); // returns [ids] of all expired meetings
			if (expired_meetings.length == 0)
				return;
			events.emit('meeting_expired', expired_meetings);
		}, ConfigurationSettings.CheckExpiredMeetingsInterval);

	module.events = events;
		
	return module;
};
