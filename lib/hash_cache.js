var cache = {};
	
exports.get = function(key){
	return cache[key];
};

exports.set = function(key, val){
	cache[key] = val;
};

exports.clear = function(key) {
	cache[key] = null;
}

