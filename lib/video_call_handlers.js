module.exports = function (io, redis, twilio) {
	function martio_session_key(sessionId) {
		return "martio:session:" + sessionId;
	}
	
	io.sockets.on('connection', function(socket) {
		socket.on("startVideoCall", function() {
			var sessionId = socket.request.cookies['martio_session'];
			console.log("connection SESSION:" + sessionId);
			
			/*
			redis.get(martio_session_key(sessionId), function(err, session_info) {
				if(session_info == null || err) {
					console.log("No session found for session_id: " + sessionId);
					return;
				}

				var user_id = JSON.parse(session_info)['warden.user.user.key'][0];
				console.log("USERID: " + user_id);

				// Do we really need a separate join message?
				socket.on('join', function (room) {
					console.log("joinRoom: (" + room + ")");
					var clients = io.sockets.adapter.rooms[room];
					var numClients = (typeof clients !== 'undefined') ? Object.keys(clients).length : 0;
					if (numClients == 0) {
						socket.join(room);
					}
					else if (numClients == 1) {
						socket.join(room);
						socket.emit('ready', room);
						socket.broadcast.emit('ready', room);
					}
					else {
						socket.emit('full', room);
					}
				});
				socket.on('token', function () {
					console.log('token');
					twilio.tokens.create(function (err, response) {
						if (err) {
							console.log(err);
						}
						else {
							socket.emit('token', response);
						}
					});
				});
				socket.on('offer', function (offer) {
					socket.broadcast.emit('offer', offer);
				});
				socket.on('candidate', function (candidate) {
					socket.broadcast.emit('candidate', candidate);
				});
				socket.on('answer', function (answer) {
					socket.broadcast.emit('answer', answer);
				});
			});
			*/
		});
	});
}
