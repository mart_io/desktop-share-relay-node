module.exports = function (io, meetingManager) {
	meetingManager.events.on("screenshot_updated", update_meeting_screenshot);
	meetingManager.events.on("meeting_expired", disconnectExpiredMeetings);

	function getBase64Screenshot(bitmapBytes) {
		if (!bitmapBytes || bitmapBytes.length == 0)
			return null;
		var base64_string = new Buffer(bitmapBytes).toString('base64');
		return base64_string;
	}

	function update_meeting_screenshot(meeting_id, current_frame) {
		var sockets = io.sockets.adapter.rooms[meeting_id];
		if (!sockets) {
			console.log("No participants in meeting " + meeting_id + " to push updates to");
			return;
		}

		var clients = Object.keys(sockets);
		if (!clients || clients.length == 0)
			return;
		
		io.sockets.in(meeting_id).emit('update_frame', current_frame);
		console.log("Updated meeting: " + meeting_id + " with " + base64_string.length + " bytes. There are " + clients.length + " clients in the room");
	};

	function disconnectExpiredMeetings(expired_meetings){ 
		for (index = 0; index < expired_meetings.length; index++) {
			var meeting_id = expired_meetings[index];
			sockets = io.sockets.adapter.rooms[meeting_id];
			if(!sockets || sockets.length == 0) return;
			var clients = Object.keys(sockets);
			console.log("Meeting: " + meeting_id + " has expired; terminating all connections to room");
			
			io.sockets.in(meeting_id).emit("meeting_ended", meeting_id);

			// disconnect all clients attached to the meeting room
			for (i = 0; i < clients.length; i++) {
				var socket_id = clients[i];
				console.log("Disconnecting " + socket_id + " from " + meeting_id);
				io.sockets.connected[socket_id].disconnect();
			}		
		}
	}
	
	io.sockets.on('connection', function (socket) {
		socket.on('join_meeting', function (meeting_id) {
			meeting = meetingManager.get_meeting(meeting_id);
			if (!meeting) {
				console.log("join_meeting: " + meeting_id + " not a valid meeting ID");
				socket.emit('meeting_not_found', "No meeting found for " + meeting_id)
				socket.disconnect();
				return;
			}

			if (new Date().getTime() > meeting.end_date.getTime()) {
				console.log("join_meeting: " + meeting_id + "  declined - meeting has already concluded");
				socket.emit('meeting_finished', null);
				socket.disconnect();
				return;
			}

			socket.join(meeting_id);
			socket.emit('meeting_connected', meeting_id);
			console.log("Joining meeting ID " + meeting_id + ". SocketID is " + socket.id);

			// initial screenshot to the newly joined client if joining an existing meeting
			var screenshot_frame = getBase64Screenshot(meeting.current_frame);
			if (!screenshot_frame) return;

			socket.emit('update_frame', screenshot_frame);
		});
	});
};
